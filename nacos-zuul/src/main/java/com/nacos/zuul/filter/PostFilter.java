package com.nacos.zuul.filter;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.nacos.commonutil.constant.CodeEnum;
import com.nacos.commonutil.vo.ResponseMsg;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: zlj
 * @Date: 2021/3/22 19:09
 * @Description:
 *   在route和error过滤器之后被调用
 */
@Slf4j
@Component
public class PostFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();

        ResponseMsg responseMsg = new ResponseMsg();
        /* 当api不存在时，统一返回 */
        if (response.getStatus() == HttpStatus.NOT_FOUND.value()) {
            responseMsg.setStatus(CodeEnum.API_NOT_FOUND.getCode());
            String msg = String.format(CodeEnum.API_NOT_FOUND.getMsg(),request.getServletPath());
            responseMsg.setDesc(msg);

            log.info(msg);
            try {
                ctx.setResponseStatusCode(HttpStatus.NOT_FOUND.value());
                ctx.addZuulResponseHeader("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
                ctx.setResponseBody(JSONUtils.serializeObject(responseMsg));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
