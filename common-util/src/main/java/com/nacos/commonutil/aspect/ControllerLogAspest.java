package com.nacos.commonutil.aspect;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: zlj
 * @Date: 2021/3/22 17:21
 * @Description:
 *      控制台输出访问的API接口信息
 */
@Slf4j
@Aspect
@Configuration
public class ControllerLogAspest {

    @Pointcut("execution(public * com.nacos.*.controller..*(..))")
    public void execudeService(){}

    @Around("execudeService()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        String url = request.getRequestURL().toString();
        String method = request.getMethod();
        String queryString = request.getQueryString();
        Object[] args = pjp.getArgs();
        String params = "";
        //获取请求参数集合并进行遍历拼接
        if(args.length>0){
            if("POST".equals(method)){
                Object object = args[0];
                log.info("请求参数object:"+object);
            }else if("GET".equals(method)){
                params = queryString;
            }
        }
        log.info("=======================================");
        log.info("=  Request URL: "+url);
        log.info("=  Type: "+method);
        log.info("=  Params: "+params);

        // result的值就是被拦截方法的返回值
        Object result = pjp.proceed();
        log.info("=  return: " + JSON.toJSONString(result));
        log.info("=======================================");
        return result;
    }
}
