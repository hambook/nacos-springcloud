package com.nacos.commonutil.vo;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.nacos.commonutil.constant.CodeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.BindingResult;

import java.io.Serializable;

/**
 * @Author: liujian
 * @Date: 2019/10/25 14:01
 * @Description:
 *         统一返回格式
 */
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMsg<T> implements Serializable {

    @ApiModelProperty(value = "状态码",required = true,dataType = "Integer",example = "0")
    private int status;

    @ApiModelProperty(value = "错误码",required = true,dataType = "Integer",example = "0")
    private int errCode;

    @ApiModelProperty(value = "错误信息",required = true,dataType = "String",example = "0")
    private String errMsg;

    @ApiModelProperty(value = "内容数据",required = false)
    private T body;

    @ApiModelProperty(value = "错误信息",required = false,dataType = "String")
    private String desc;

    public void setCodeEnum(CodeEnum codeEnum){
        this.status = codeEnum.getCode();
        this.desc = codeEnum.getMsg();
    }

    public ResponseMsg(int status, int errCode, String desc, T body) {
        this.errCode = errCode;
        this.status = status;
        this.body = body;
        this.desc = desc;
    }

    public ResponseMsg(int status, String desc, T body) {
        this.status = status;
        this.body = body;
        this.desc = desc;
    }

    public ResponseMsg(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public static ResponseMsg ok() {
        return new ResponseMsg(CodeEnum.SUCCESS.getCode(), CodeEnum.SUCCESS.getMsg(),new JSONObject());
    }

    public static ResponseMsg ok(Object body) {
        return new ResponseMsg(CodeEnum.SUCCESS.getCode(), CodeEnum.SUCCESS.getMsg(), body);
    }

    public static ResponseMsg fail() {
        return new ResponseMsg(CodeEnum.FAIL.getCode(), CodeEnum.FAIL.getMsg(),new JSONObject());
    }

    public static ResponseMsg fail(Object body) {
        return new ResponseMsg(CodeEnum.FAIL.getCode(), CodeEnum.FAIL.getMsg(), body);
    }

    public static ResponseMsg fail(Integer code,String desc) {
        return new ResponseMsg(code, desc,new JSONObject());
    }

    public static ResponseMsg fail(CodeEnum codeEnum){
        return new ResponseMsg(codeEnum.getCode(), codeEnum.getMsg());
    }

    /**
     * 请求失败返回 （校验返回）
     */
    public static ResponseMsg faild(BindingResult errors) {
        ResponseMsg rd = new ResponseMsg();
        rd.setStatus(CodeEnum.REQUEST_PARAM_ERROR.getCode());
        errors.getAllErrors().stream().forEach(error -> {
            rd.setDesc(error.getDefaultMessage());
        });
        return rd;
    }
}
