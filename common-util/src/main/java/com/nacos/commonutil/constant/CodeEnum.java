package com.nacos.commonutil.constant;

import lombok.Getter;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ResponseMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zlj
 * @Date: 2021/3/22 17:20
 * @Description:
 *      返回状态信息枚举
 */
@Getter
public enum CodeEnum {

    /** 操作失败 **/
    FAIL("ERROR:Fail", -1),
    /** 操作成功 **/
    SUCCESS("Success", 0),

    API_NOT_FOUND("API不存在: %s", 103),
    API_BEING_INTERCEPTED("API:%s已被拦截,请授权！",107),
    DATA_NOT_FOUND("找不到数据",106),

    ROUTE_ERROR("路由失败",105),

    SESSION_TIME_OUT("session已过期",201),
    EXCEPTION_SYS("系统异常: %s", 999),

    /**
     * 请求参数
     */
    REQUEST_PARAM_NULL("请求参数 %s 不能为空", 400),

    REQUEST_PARAM_ERROR("请求参数 %s 格式错误", 401),
    ;

    private String msg;
    private int code;

    CodeEnum(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    // 解析CodeEnum错误编码
    public static List<ResponseMessage> parseErrorCode(){
        List<ResponseMessage> responseMessageList = new ArrayList<>();
        ResponseMessageBuilder responseMessageBuilder = new ResponseMessageBuilder();
        CodeEnum[] codeEnums = CodeEnum.values();
        for (CodeEnum codeEnum : codeEnums){
            responseMessageList.add(responseMessageBuilder.code(codeEnum.getCode()).message(codeEnum.getMsg()).build());
        }
        return responseMessageList;
    }
}
